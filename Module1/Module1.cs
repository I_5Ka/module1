﻿using System;

namespace M1
{
    public class Module1
    {
        static void Main(string[] args)
        {
            /* Module1 */
        }


        public int[] SwapItems(int a, int b)
        {
            int tmp;
            tmp = a;
            a = b;
            b = tmp;
            int[] c = { a, b };
            return c;
        }

        public int GetMinimumValue(int[] input)
        {
            int min;
            min = input[0];
            for (int i = 0; i < input.Length; i++)
            {
                if (min > input[i])
                {
                    min = input[i];
                }
            }
            return (min);
        }
    }
}
